import React from 'react'
import { Link } from 'react-router-dom'
import {Row, Col} from 'reactstrap'

export default class Product extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            productArray:[]
        }
    }

    componentDidMount(){
        fetch('https://mpbinarnight.herokuapp.com/product')
        .then(response=>response.json())
        .then(data=>{
            this.setState(state=>({
                productArray: [...state.productArray, ...data]
            }))
        })
    }

    render() {
        //get()
        return (
            <div>
                    {
                        this.state.productArray.map(product=>{
                            return(
                                <li>
                                    <Link to={`/product/${product.id}`}>
                                    <img src={product.categories} alt=""/>
                                    <p>{product.title}</p>
                                    </Link>
                                </li>
                            )
                        })
                    }
            </div>
                )
            }
}